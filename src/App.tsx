import React from 'react';
import UsersListContainer from './Containers/UsersList.container'
import './App.css';

const App: React.FC = () => (
  <div className="App">
    <UsersListContainer />
  </div>
)

export default App
