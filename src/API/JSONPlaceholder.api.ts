export interface Geo {
    lat: string
    lng: string
}

export interface UserAddress {
    city: string
    geo: Geo
    street: string
    suite: string
    zipcode: string
}

export interface UserCompany {
    name: string
    catchPhrase: string
    bs: string
}

export interface User {
    address: UserAddress
    company: UserCompany
    email: string
    id: number
    name: string
    phone: string
    username: string
    website: string
}

class JSONPlaceholderAPI {

    private abortController?: AbortController
    private controllerSignal?: AbortSignal

    constructor (
        public protocol: string = 'https',
        public hostname: string = 'jsonplaceholder.typicode.com'
    ) {

        if (AbortController) {
            this.abortController = new AbortController()
            this.controllerSignal = this.abortController.signal
        }
    }

    abortRequest (): void {
        this.abortController && this.abortController.abort()
    }

    getUsers (): Promise<User[]> {
        return this.request<User[]>('users')
    }

    private request <R> (path: string, method: 'GET' | 'POST' = 'GET'): Promise<R> {
        const url = `${this.protocol}://${this.hostname}/${path}`
        const headers: Headers = new Headers()

        headers.append('Content-Type', 'application/json; charset=UTF-8')

        return fetch(url, { method, headers, signal: this.controllerSignal })
            .then(result => result.json())
    }
}

export default JSONPlaceholderAPI
