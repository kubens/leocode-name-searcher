import fetchMock from 'fetch-mock'
import JSONPlaceholderAPI, { User } from './JSONPlaceholder.api'

const USER_0_MOCK: User = {
    address: {
        city: '__CITY__',
        geo: { 
            lat: '__LAT__', 
            lng: '__LONG__' 
        },
        street: '__STREET__',
        suite: '__SUITE__',
        zipcode: '__ZIP_CODE__'
    },
    company: { 
        bs: '__BS__', 
        catchPhrase: '__CATCH_PHRASE__', 
        name: '__COMPANY_NAME__' 
    },
    email: '__EMAIL__',
    id: 0,
    name: '__NAME__',
    phone: '__PHONE__',
    username: '__USERNAME__',
    website: '__WEBSITE__'
}

describe('JSON Placeholder API', () => {

    let provider: JSONPlaceholderAPI

    beforeEach(() => {
        provider = new JSONPlaceholderAPI()
    })

    afterAll(() => {
        fetchMock.reset()
    })

    it('returns proper users list', async () => {
        fetchMock.get('https://jsonplaceholder.typicode.com/users', {
            status: 200,
            body: JSON.stringify([USER_0_MOCK])
        })

        expect(await provider.getUsers()).toEqual([USER_0_MOCK])
    })
})