import React from 'react'

export function useMountEffect (): () => boolean {
    const ref = React.useRef(false)
    const get = React.useCallback(() => ref.current, [])

    React.useEffect(() => {
        ref.current = true

        return () => {
            ref.current = false
        }
    })

    return get
}