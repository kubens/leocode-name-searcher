import React from 'react'
import styles from './Loader.module.css'

const Spinner: React.FC = () => (
    <div className={styles.loader}></div>
)

export default Spinner
