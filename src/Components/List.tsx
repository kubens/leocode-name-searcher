import React from 'react'
import styles from './List.module.css'

export interface ListElement {
    title: string
    subtitle?: string
}

type ListProps = {
    elements: ListElement[]
}

const List: React.FC<ListProps> = ({
    elements
}) => (
    <ol className={styles.list}>
        {elements.map((element, index) => (
            <li key={`${index}-list-item`} className={styles.listItem}>
                <span className={styles.title}>
                    {element.title}
                </span>

                {element.subtitle && (
                    <span className={styles.subtitle}>
                        {element.subtitle}
                    </span>
                )}
            </li>
        ))}
    </ol>
)

export default List
