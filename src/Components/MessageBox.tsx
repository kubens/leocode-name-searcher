import React from 'react'
import styles from './MessageBox.module.css'

type EmptyResultsProps = {
    title: string
    description?: string
}

const MessageBox: React.FC<EmptyResultsProps> = ({
    title,
    description
}) => (
    <div className={styles.container}>
        <h2>{title}</h2>
        { description && <p>{description}</p> }
    </div>
)

export default MessageBox