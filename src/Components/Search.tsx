import React from 'react'
import styles from './Search.module.css'

type SearchProps = {
    disable: boolean
    placeholder?: string
    value: string
    onValueChange?: (value: string) => void
}

const Search: React.FC<SearchProps> = (({
    disable,
    placeholder,
    value,
    onValueChange
}) => {

    function onChange (event: React.ChangeEvent<HTMLInputElement>): void {
        onValueChange && onValueChange(event.target.value)
    }

    return (
        <div className={styles.container}>
            <input 
                className={styles.input}
                disabled={disable} 
                placeholder={placeholder} 
                value={value} 
                onChange={onChange}
            />
        </div>
    )
})

export default Search