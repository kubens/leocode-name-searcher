import React from 'react'
import JSONPlaceholderAPI, { User } from '../API/JSONPlaceholder.api'
import { useMountEffect } from '../Effects'
import Loader from '../Components/Loader'
import Search from '../Components/Search'
import MessageBox from '../Components/MessageBox'
import List, { ListElement } from '../Components/List'

// Create API once
const api = new JSONPlaceholderAPI()

const UserListContainer: React.FC = () => {

    const isMounted = useMountEffect()
    const [fetchedUsers, setFetchedUsers] = React.useState<User[]>([])
    const [hasError, setError] = React.useState(false)
    const [isLoading, setLoadingState] = React.useState(false)
    const [listElements, setListElements] = React.useState<ListElement[]>([])
    const [query, setQuery] = React.useState('')


    React.useEffect(() => {
        setLoadingState(true)
        
        api.getUsers()
            .then(users => isMounted() && setFetchedUsers(users))
            .catch(() => isMounted() && setError(true))
            .finally(() => isMounted() && setLoadingState(false))

        return () => {
            api.abortRequest()
        }

    }, [isMounted])

    React.useEffect(() => {
        const queryResult = fetchedUsers.reduce((prev: ListElement[], user) => {
            if (user.name.indexOf(query.trim()) > -1) {
                prev.push({
                    title: user.name,
                    subtitle: `@${user.username}`
                })
            }

            return prev
        }, [] as ListElement[])

        setListElements(queryResult)

    }, [fetchedUsers, query])

    function isListVisible (): boolean {
        return (!isLoading && !hasError) && !!listElements.length
    }

    function isEmptyListVisible (): boolean {
        return (!isLoading && !hasError) && !listElements.length
    }

    return (
        <React.Fragment>
            <header>
                <h1 className="text-center">Users list</h1>
            </header>

            <div className="search-bar">
                <Search
                    disable={isLoading} 
                    placeholder="Search by user name..." 
                    value={query}
                    onValueChange={setQuery}
                />
            </div>

            <div className="content">
                { isLoading && (
                    <Loader />
                )}

                { hasError && (
                    <MessageBox
                        title="Something goes wrong"
                        description="Houston we have a problem! Try refresh page."
                    />
                )}
                
                { isListVisible() && (
                    <List elements={listElements} />
                )}
                
                { isEmptyListVisible() && (
                    <MessageBox
                        title="No results 🥺" 
                        description={`Try to find other word than "${query.trim()}"`} 
                    />
                )}
            </div>
        </React.Fragment>
    )
}

export default UserListContainer